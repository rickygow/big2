﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card {

    private int value;
    private CardSymbol cardSymbol;
    private int cardRank;

    public Sprite cardImage;
    public Sprite backCardImage;

    public Card(int val, CardSymbol cardSym, int rank, Sprite cardImage, Sprite backCardImage)
    {
        value = val;
        cardSymbol = cardSym;
        cardRank = rank;

        this.cardImage = cardImage;
        this.backCardImage = backCardImage;
    }

    public int getValue()
    {
        return value;
    }

    public CardSymbol getSymbol()
    {
        return cardSymbol;
    }

    public int getRank()
    {
        return cardRank;
    }

    public void setRank(int val)
    {
        cardRank = val;
    }

    public static int SortByRank(Card c1, Card c2)
    {
        return c1.cardRank.CompareTo(c2.cardRank);
    }

    public enum CardSymbol{
        DIAMOND, CLUB, HEART, SPADE
    }

    public enum CARDSET
    {
        INVALID, SINGLE, PAIR, THREEOFAKIND, STRAIGHT, FLUSH, FULLHOUSE_L, FULLHOUSE_R, FOUROFAKIND_L, FOUROFAKIND_R, STRAIGHTFLUSH
    }

    public static void printSetOfCards(List<Card> soc)
    {
        Debug.Log("================================================================");
        for (int i = 0; i < soc.Count; i++)
        {
            Debug.Log("-> [" + soc[i].getRank() + "] " + soc[i].getValue() + " " + soc[i].getSymbol().ToString());
        }
        Debug.Log("================================================================");
    }

    public static void printAllSetOfCards(List<List<Card>> soc)
    {
        Debug.Log("=================LIST OF LIST OF CARDS==========================");

        for (int i = 0; i < soc.Count; i++)
        {
            string text = "["+i+"] -> ";
            for (int ii = 0; ii < soc[i].Count; ii++)
            {
                text += soc[i][ii].getValue() + " " + soc[i][ii].getSymbol() + "  |  ";
            }
            Debug.Log(text);
        }

        Debug.Log("================================================================");
    }

    public static void printAllSetOfCards(List<CardCombination> soc)
    {
        Debug.Log("=================LIST OF LIST OF CARDS==========================");

        for (int i = 0; i < soc.Count; i++)
        {
            string text = "[" + i + "] -> ";
            for (int ii = 0; ii < soc[i].cards.Count; ii++)
            {
                text += soc[i].cards[ii].getValue() + " " + soc[i].cards[ii].getSymbol() + "  |  ";
            }
            text += "[R: " + soc[i].highRankValue + "]";
            Debug.Log(text);
        }

        Debug.Log("================================================================");
    }

    public static CARDSET AnalyzeCardSet(List<Card> soc)
    {
        soc.Sort(Card.SortByRank);

        //Card.printSetOfCards(soc);

        switch (soc.Count)
        {
            case 1:
                //Single Card
                return CARDSET.SINGLE;
            case 2:
                //Pair
                if (soc[0].value == soc[1].value)
                    return CARDSET.PAIR;
                else
                    return CARDSET.INVALID;
            case 3:
                //Three-Of-A-Kind
                if (soc[0].value == soc[1].value && soc[0].value == soc[2].value)
                    return CARDSET.THREEOFAKIND;
                else
                    return CARDSET.INVALID;
            case 5:
                //Straight
                if( soc[1].value == (soc[0].value + 1) && 
                    soc[2].value == (soc[0].value + 2) && 
                    soc[3].value == (soc[0].value + 3) &&
                    soc[4].value == (soc[0].value + 4))
                {
                    //Straight Flush
                    if (soc[1].cardSymbol == soc[0].cardSymbol &&
                        soc[2].cardSymbol == soc[0].cardSymbol &&
                        soc[3].cardSymbol == soc[0].cardSymbol &&
                        soc[4].cardSymbol == soc[0].cardSymbol)
                    {
                        return CARDSET.STRAIGHTFLUSH;
                    }
                    return CARDSET.STRAIGHT;
                }
                //Flush
                if (soc[1].cardSymbol == soc[0].cardSymbol &&
                    soc[2].cardSymbol == soc[0].cardSymbol &&
                    soc[3].cardSymbol == soc[0].cardSymbol &&
                    soc[4].cardSymbol == soc[0].cardSymbol)
                {
                    return CARDSET.FLUSH;
                }
                //FULL HOUSE (0, 0, 1, 1, 1)
                if (soc[1].value == soc[0].value &&
                    soc[3].value == soc[2].value &&
                    soc[4].value == soc[2].value)
                {
                    return CARDSET.FULLHOUSE_R;
                }
                //FULL HOUSE (1, 1, 1, 0, 0)
                if (soc[1].value == soc[0].value &&
                    soc[2].value == soc[0].value &&
                    soc[3].value == soc[4].value)
                {
                    return CARDSET.FULLHOUSE_L;
                }
                //FOUR OF A KIND (1, 1, 1, 1, 0)
                if (soc[1].value == soc[0].value &&
                    soc[2].value == soc[0].value &&
                    soc[3].value == soc[0].value)
                {
                    return CARDSET.FOUROFAKIND_L;
                }
                //FOUR OF A KIND (0, 1, 1, 1, 1)
                if (soc[1].value == soc[4].value &&
                    soc[2].value == soc[4].value &&
                    soc[3].value == soc[4].value)
                {
                    return CARDSET.FOUROFAKIND_R;
                }
                return CARDSET.INVALID;
            default:
                //Invalid
                return CARDSET.INVALID;

        }
    }

 }
