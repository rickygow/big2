﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{

    private CardManager cardManager;
    private List<Card> shuffledCard;

    public PlayerStatus player1;
    public PlayerStatus player2;
    public PlayerStatus player3;
    public PlayerStatus player4;

    public Text cardCombinationText;
    public Text informationText;

    public List<GameObject> player1CardsGO;
    public List<GameObject> collectedCardGO;

    public Transform playerHandsParent;
    public Transform submitCardParent;

    public GameObject cardPrefab;
    public GameObject collectedCardPrefab;

    public List<GameObject> selectedCardGameObject;

    public Button playButton;
    public Button skipButton;

    private int roundCardCount;
    private List<Card> collectedCard;

    #region Singleton
    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }
    #endregion

    // Use this for initialization
    void Start()
    {
        cardManager = CardManager.Instance;
        cardManager.ResetDeckOfCards();

        selectedCardGameObject = new List<GameObject>();

        //Card.printSetOfCards(cardManager.deckOfCards);

        shuffledCard = new List<Card>(CardManager.ShuffleCards(cardManager.deckOfCards));

        player1 = new PlayerStatus();
        player2 = new PlayerStatus();
        player3 = new PlayerStatus();
        player4 = new PlayerStatus();

        // Distribute Cards
        DistributeCardFromTopList(shuffledCard, player1.card, 13);
        DistributeCardFromTopList(shuffledCard, player2.card, 13);
        DistributeCardFromTopList(shuffledCard, player3.card, 13);
        DistributeCardFromTopList(shuffledCard, player4.card, 13);

        //Card.printSetOfCards(player1Cards);

        // initialize Card from Card List
        player1CardsGO = new List<GameObject>();

        // initialize Collected Card
        collectedCardGO = new List<GameObject>();


        foreach (Card card in player1.card)
        {
            GameObject temp = GameObject.Instantiate(cardPrefab, playerHandsParent);
            temp.GetComponent<Transform>().localScale = new Vector3(0.65f, 1f, 1f);
            temp.GetComponent<Image>().sprite = card.cardImage;
            temp.GetComponent<CardObjectManager>().card = card;
            player1CardsGO.Add(temp);
        }

        player1.playerState = PlayerState.WAIT;
        player2.playerState = PlayerState.WAIT;
        player3.playerState = PlayerState.WAIT;
        player4.playerState = PlayerState.WAIT;

        //Find 3 Diamonds (start coroutine 2sec for waiting anim)
        if(player1.card[0].getRank() == 8)
        {
            informationText.text = "Player 1 Turn";
            player1.playerState = PlayerState.START_ROUND;
        }
        if (player2.card[0].getRank() == 8)
        {
            player2.playerState = PlayerState.START_ROUND;
            player2Start();
        }
        if (player3.card[0].getRank() == 8)
        {
            informationText.text = "Player 3 Turn";
        }
        if (player4.card[0].getRank() == 8)
        {
            player4.playerState = PlayerState.START_ROUND;
            player4Start();
        }

        FindAllPossiblePairCard(player1.card);
        FindAllPossibleTripletCard(player1.card);
        FindAllPossibleFourOfAKind(player1.card);
        FindAllPossibleStraightCard(player1.card);
    }

    public void Player1CollectCard()
    {
        int selectedCardCount = player1.selectedCard.Count;

        if(player1.playerState == PlayerState.START_ROUND)
        {
            roundCardCount = selectedCardCount;
            collectedCard = new List<Card>(player1.selectedCard);
            SpawnCollectedCard(collectedCard);
        }

        player1.playerState = PlayerState.WAIT;
        playButton.interactable = false;
        skipButton.interactable = false;

        player2.playerState = PlayerState.CONTINUE_ROUND;
        player2Start();
    }

    public void player2Start()
    {
        informationText.text = "Player 2 Turn";

        if (player2.playerState == PlayerState.START_ROUND)
        {
            //List<CardCombination> pairCard = FindAllPossiblePairCard(player2.card);
            //List<CardCombination> tripletCard = FindAllPossiblePairCard(player2.card);

            if (player2.card[0].getRank() == 8) // IF had 3 DIAM
            {

            }
        }
    }

    public void player4Start()
    {
        informationText.text = "Player4 Turn";

        if (player4.playerState == PlayerState.START_ROUND)
        {
            //List<CardCombination> pairCard = FindAllPossiblePairCard(player4.card);
            //List<CardCombination> tripletCard = FindAllPossiblePairCard(player2.card);

            if (player4.card[0].getRank() == 8) // IF had 3 DIAM
            {

            }
        }
    }

    public List<CardCombination> FindAllPossiblePairCard(List<Card> soc)
    {
        List<CardCombination> result = new List<CardCombination>();

        if (soc.Count < 2)
            return result;
         
        // (0,P,P,0,0) 
        for(int i = 0; i < (soc.Count - 1); i++)
        {
            List<Card> newPairSet = new List<Card>();
            if(soc[i].getValue() == soc[i + 1].getValue())
            {
                newPairSet.Add(soc[i]);
                newPairSet.Add(soc[i+1]);
                CardCombination newCardCombination = new CardCombination(newPairSet);
                result.Add(newCardCombination);
            }
        }


        if (soc.Count < 3)
            return result;

        // (0,P,P,P,0)
        for (int i = 0; i < (soc.Count - 2); i++)
        {
            List<Card> newPairSet = new List<Card>();
            if (soc[i].getValue() == soc[i + 2].getValue())
            {
                newPairSet.Add(soc[i]);
                newPairSet.Add(soc[i + 2]);
                CardCombination newCardCombination = new CardCombination(newPairSet);
                result.Add(newCardCombination);
            }
        }

        if (soc.Count < 4)
            return result;

        // (0,P,P,P,P)
        for (int i = 0; i < (soc.Count - 3); i++)
        {
            List<Card> newPairSet = new List<Card>();
            if (soc[i].getValue() == soc[i + 3].getValue())
            {
                newPairSet.Add(soc[i]);
                newPairSet.Add(soc[i + 3]);
                CardCombination newCardCombination = new CardCombination(newPairSet);
                result.Add(newCardCombination);
            }
        }

        result.Sort(CardCombination.SortByHighRankValue);
        Card.printAllSetOfCards(result);
        return result;
    }

    public List<CardCombination> FindAllPossibleTripletCard(List<Card> soc)
    {
        List<CardCombination> result = new List<CardCombination>();

        if (soc.Count < 3)
            return result;

        // (0,P,P,P,0)
        for (int i = 0; i < (soc.Count - 2); i++)
        {
            List<Card> newPairSet = new List<Card>();
            if (soc[i].getValue() == soc[i + 1].getValue() && soc[i].getValue() == soc[i + 2].getValue())
            {
                newPairSet.Add(soc[i]);
                newPairSet.Add(soc[i + 1]);
                newPairSet.Add(soc[i + 2]);
                CardCombination newCardCombination = new CardCombination(newPairSet);
                result.Add(newCardCombination);
            }
        }

        if (soc.Count < 4)
            return result;

        // (0,P,P,X,P)
        for (int i = 0; i < (soc.Count - 4); i++)
        {
            List<Card> newPairSet = new List<Card>();
            if (soc[i].getValue() == soc[i + 1].getValue() && soc[i].getValue() == soc[i + 3].getValue())
            {
                newPairSet.Add(soc[i]);
                newPairSet.Add(soc[i + 1]);
                newPairSet.Add(soc[i + 2]);
                CardCombination newCardCombination = new CardCombination(newPairSet);
                result.Add(newCardCombination);
            }
        }

        // (0,P,X,P,P)
        for (int i = 0; i < (soc.Count - 4); i++)
        {
            List<Card> newPairSet = new List<Card>();
            if (soc[i].getValue() == soc[i + 2].getValue() && soc[i].getValue() == soc[i + 3].getValue())
            {
                newPairSet.Add(soc[i]);
                newPairSet.Add(soc[i + 1]);
                newPairSet.Add(soc[i + 2]);
                CardCombination newCardCombination = new CardCombination(newPairSet);
                result.Add(newCardCombination);
            }
        }

        result.Sort(CardCombination.SortByHighRankValue);
        Card.printAllSetOfCards(result);
        return result;
    }

    public List<CardCombination> FindAllPossibleFourOfAKind(List<Card> soc)
    {
        List<CardCombination> result = new List<CardCombination>();

        if (soc.Count < 4)
            return result;

        for (int i = 0; i < (soc.Count - 3); i++)
        {
            List<Card> newPairSet = new List<Card>();
            if (soc[i].getValue() == soc[i + 1].getValue() && soc[i].getValue() == soc[i + 2].getValue() && soc[i].getValue() == soc[i + 3].getValue())
            {
                newPairSet.Add(soc[i]);
                newPairSet.Add(soc[i + 1]);
                newPairSet.Add(soc[i + 2]);
                newPairSet.Add(soc[i + 4]);
                CardCombination newCardCombination = new CardCombination(newPairSet);
                result.Add(newCardCombination);
            }
        }

        result.Sort(CardCombination.SortByHighRankValue);
        Card.printAllSetOfCards(result);
        return result;
    }

    public List<CardCombination> FindAllPossibleStraightCard(List<Card> soc)
    {
        List<CardCombination> result = new List<CardCombination>();

        if (soc.Count < 4)
            return result;

        for (int i = (soc.Count-1); i > 4; i--)
        {
            //Debug.Log("======= Current Index [" + i + "] =============");

            List<Card> newPairSet = new List<Card>();
            newPairSet.Add(soc[i]);

            int j = 1;
            while(i - j > 0)
            {
                
                // Best Case

                int a = soc[i - j].getValue();
                int b = newPairSet[0].getValue();
                #region forceAceTwos
                if (a == 1 || a == 2)
                    a += 13;
                if (b == 1 || b == 2)
                    b += 13;
                #endregion
                //int b = newPairSet[newPairSet.Count - 1].getValue();

                //Debug.Log("Current Check Index: " + (i - j) + " Current Index Value: " + b + " COMPARE WITH: " + a);

                if (a == (b - newPairSet.Count))
                {
                    newPairSet.Add(soc[i - j]);
                    //Debug.Log("Pass!, new Pair Count: " + newPairSet.Count);
                    if (newPairSet.Count == 5)
                    {
                        CardCombination newCardCombination = new CardCombination(newPairSet);
                        result.Add(newCardCombination);
                        break;
                    }
                }

                if(a == b)
                {
                    newPairSet[newPairSet.Count - 1] = soc[i - j];
                }

                j++;
            }
        }


        result.Sort(CardCombination.SortByHighRankValue);
        Card.printAllSetOfCards(result);
        return result;
    }


    public void SpawnCollectedCard(List<Card> collectedCard)
    {
        foreach (Transform child in submitCardParent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        collectedCardGO.Clear();

        Debug.Log("Destroy!");
        foreach (GameObject go in selectedCardGameObject)
        {
            GameObject.Destroy(go);
        }
        selectedCardGameObject.Clear();
        player1.selectedCard.Clear();

        foreach (Card card in collectedCard)
        {
            GameObject temp = GameObject.Instantiate(collectedCardPrefab, submitCardParent);
            temp.GetComponent<Transform>().localScale = new Vector3(0.65f, 1f, 1f);
            temp.GetComponent<Image>().sprite = card.cardImage;
            temp.GetComponent<CardObjectManager>().card = card;
            collectedCardGO.Add(temp);
        }
    }

    // Show Player 1 Hand Combination Set
    public void AnalyzePlayer1HandCard()
    {
        Card.CARDSET cardSet = Card.AnalyzeCardSet(player1.selectedCard);
        cardCombinationText.text = cardSet.ToString();

        if (player1.playerState == PlayerState.START_ROUND)
        {
            if (cardSet != Card.CARDSET.INVALID)
            {
                playButton.interactable = true;
                skipButton.interactable = true;
            }
            else
            {
                playButton.interactable = false;
                skipButton.interactable = true;
            }
        }
        else if (player1.playerState == PlayerState.CONTINUE_ROUND)
        {
            if (cardSet != Card.CARDSET.INVALID)
            {
                //Check current table card
                //Check selected card count
                //Check if selected card set is higher
                skipButton.interactable = true;
            }
            else
            {
                playButton.interactable = false;
                skipButton.interactable = true;
            }
        }
        else if (player1.playerState == PlayerState.WAIT || player1.playerState == PlayerState.SKIPPED)
        {
            playButton.interactable = false;
            skipButton.interactable = false;
        }
        else
        {
            playButton.interactable = false;
            skipButton.interactable = false;
        }

        //Debug.Log(cardSet.ToString());
    }

    public void DistributeCardFromTopList(List<Card> deckOfCard, List<Card> targetHand, int count)
    {
        for (int i = 0; i < count; i++)
        {
            targetHand.Add(deckOfCard[i]);
        }
        targetHand.Sort(Card.SortByRank);
        deckOfCard.RemoveRange(0, count);
    }
}