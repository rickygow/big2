﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus {

    public List<Card> card;
    public List<Card> selectedCard;
    public PlayerState playerState;

    public PlayerStatus()
    {
        card = new List<Card>();
        selectedCard = new List<Card>();
        playerState = PlayerState.WAIT;
    }

}

public enum PlayerState
{
    START_ROUND,
    CONTINUE_ROUND,
    SKIPPED,
    WAIT
}
