﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardCombination {

    public List<Card> cards;
    public int cardCount;
    public Card.CARDSET cardSetType;
    public int highRankValue;
    
    public CardCombination(List<Card> soc)
    {
        cards = new List<Card>(soc);

        cardCount = soc.Count;
        cards.Sort(Card.SortByRank);
        cardSetType = Card.AnalyzeCardSet(cards);
        if (cardSetType == Card.CARDSET.FULLHOUSE_L)
        {
            highRankValue = soc[2].getRank();
        }
        else if (cardSetType == Card.CARDSET.FOUROFAKIND_L)
        {
            highRankValue = soc[3].getRank();
        }
        else
        {
            highRankValue = soc[soc.Count - 1].getRank();
        }
    }

    public static int SortByHighRankValue(CardCombination c1, CardCombination c2)
    {
        return c1.highRankValue.CompareTo(c2.highRankValue);
    }
}
