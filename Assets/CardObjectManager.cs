﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardObjectManager : MonoBehaviour {

    public Card card;
    public Image selectedIcon;

    private bool selected;
    
    public void SelectCard()
    {
        if (!selected)
        {
            if(GameManager.Instance.player1.selectedCard.Count < 5)
            {
                GameManager.Instance.player1.selectedCard.Add(this.card);
                GameManager.Instance.selectedCardGameObject.Add(this.gameObject);
                GameManager.Instance.AnalyzePlayer1HandCard();
                selectedIcon.gameObject.SetActive(true);
                selected = true;
            }
        }
        else if (card.getRank() != 8)
        {
            GameManager.Instance.player1.selectedCard.Remove(this.card);
            GameManager.Instance.selectedCardGameObject.Remove(this.gameObject);
            GameManager.Instance.AnalyzePlayer1HandCard();
            selectedIcon.gameObject.SetActive(false);
            selected = false;
        }

        //Card.printSetOfCards(GameManager.Instance.player1SelectedCards);
    }

    public void Start()
    {
        
    }

}
