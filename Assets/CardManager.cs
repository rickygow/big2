﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardManager : MonoBehaviour {

    public List<Card> deckOfCards;
    public Sprite[] cardImage;

    #region Singleton
    public static CardManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public void ResetDeckOfCards()
    {
        deckOfCards = new List<Card>();
        cardImage = Resources.LoadAll<Sprite>("Sprites/cards");

        //Adding Cards to deck of Cards

        int rank = 0;

        rank = 0;
        for (int i = 0; i < 13; i++)
        {
            deckOfCards.Add(new Card(((i % 13) + 1), Card.CardSymbol.DIAMOND, rank, cardImage[i], cardImage[54]));
            rank += 4;
        }

        rank = 1;
        for (int i = 13; i < 26; i++)
        {
            deckOfCards.Add(new Card(((i % 13) + 1), Card.CardSymbol.CLUB, rank, cardImage[i + 13], cardImage[54]));
            rank += 4;
        }

        rank = 2;
        for (int i = 26; i < 39; i++)
        {
            deckOfCards.Add(new Card(((i % 13) + 1), Card.CardSymbol.HEART, rank, cardImage[i - 13], cardImage[54]));
            rank += 4;
        }

        rank = 3;
        for (int i = 39; i < 52; i++)
        {
            deckOfCards.Add(new Card(((i % 13) + 1), Card.CardSymbol.SPADE, rank, cardImage[i], cardImage[54]));
            rank += 4;
        }

        // Sort By Rank

        deckOfCards.Sort(Card.SortByRank);

        // Push Rank so Highest Rank is cards of 2

        for(int i = 0; i < 8; i++)
        {
            deckOfCards[i].setRank(i + 52);
        }

        deckOfCards.Sort(Card.SortByRank);
    }

    public static List<Card> ShuffleCards(List<Card> setOfCards)
    {
        // Fisher-Yates Shuffle
        List<Card> copySet = new List<Card>(setOfCards);
        List<Card> shuffledSet = new List<Card>();
        for(int i = copySet.Count; i > 1; i--)
        {
            int randomIndex = Mathf.RoundToInt(Random.value * (i-1));
            shuffledSet.Add(copySet[randomIndex]);
            copySet.RemoveAt(randomIndex);
        }
        shuffledSet.Add(copySet[0]);
        copySet.RemoveAt(0);

        //Debug.Log("size: "+shuffledSet.Count);
        //Card.printSetOfCards(shuffledSet);

        return shuffledSet;
    }

}
